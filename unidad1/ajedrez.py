print(".model small")
print(".stack 100h")
print(".data")
print(".code")
print("mov ax,03")
print("int 10h")

for y in range(0, 8):
    for x in range(0, 8):
        if (x + y) % 2 == 0:
            _x = x * 4
            _y = y * 2
            for _x_delta in range(0, 4):
                for _y_delta in range(0, 2):
                    print("mov dl,{0}".format(24 + _x + _x_delta))
                    print("mov dh,{0}".format(4 + _y + _y_delta))
                    print("mov ah,02h")
                    print("int 10h")
                    print("mov dx,219")
                    print("mov ah,02h")
                    print("int 21h")

print(".exit")
print("end")
