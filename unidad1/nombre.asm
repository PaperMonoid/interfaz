.model small
.stack 100h
.data
.code
; writes s
mov dx,83
mov ah,02h
int 21h

; writes a
mov dx,65
mov ah,02h
int 21h

; writes n
mov dx,78
mov ah,02h
int 21h

; writes t
mov dx,84
mov ah,02h
int 21h

; writes i
mov dx,73
mov ah,02h
int 21h

; writes a
mov dx,65
mov ah,02h
int 21h

; writes g
mov dx,71
mov ah,02h
int 21h

; writes o
mov dx,79
mov ah,02h
int 21h
.exit
end