.model small
.stack 100h
.data
.code
; limpiar
mov ax,03
int 10h

; pico arriba
mov dl,42
mov dh,6
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,38
mov dh,6
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,41
mov dh,5
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,39
mov dh,5
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,40
mov dh,4
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

; pico izquierdo y derecho
mov dl,43
mov dh,6
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,37
mov dh,6
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,44
mov dh,6
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,36
mov dh,6
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,37
mov dh,7
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,43
mov dh,7
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

; picos inferiores
mov dl,36
mov dh,8
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,44
mov dh,8
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,37
mov dh,8
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,43
mov dh,8
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,38
mov dh,7
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,42
mov dh,7
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,39
mov dh,7
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,41
mov dh,7
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,40
mov dh,6
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

.exit
end
