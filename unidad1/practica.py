from PIL import Image

colors = {
    "2": (34,170,0,255),
    "6": (249,85,82,255),
    "12": (166,85,0,255),
    "0": (0,0,0,255),
    "7": (170,170,170,255),
    "15": (255,255,255,255),
    "none": (0,0,0,0)
}

def color_distance(a, b):
    return (((a[0] - b[0]) ** 2) + ((a[1] - b[1]) ** 2) + ((a[2] - b[2]) ** 2) + ((a[3] - b[3]) ** 2)) ** 0.5

def closest(a):
    distance = -1
    key = "none"
    for k, b in colors.items():
        d = color_distance(a, b)
        if distance < 0 or d < distance:
            distance = d
            key = k
    if key == "none":
        return None
    else:
       return key

im = Image.open('practicar.png') # Can be many different formats.
[width, height] = im.size
pixels = im.load()
block_size = 12

print(".model small")
print(".stack 100h")
print(".data")
print(".code")
print("mov ax,03")
print("int 10h")

for y in range(0, height, block_size):
    pixel_row = []
    for x in range(0, width, block_size):
        pixel_row.append({
            "x": int(x / block_size),
            "y": int(y / block_size),
            "color": closest(pixels[x, y])
        })

    previous = { "x": 0, "y": 0, "color": None}
    i = 1
    for pixel in pixel_row:
        if previous["color"] is None:
            previous = pixel
            i = 1
        elif pixel["color"] is previous["color"]:
            i = i + 1
        else:
            print("mov dh,{0} ; row".format(previous["y"]))
            print("mov dl,{0} ; column".format(previous["x"] * 2))
            print("mov ah,02h")
            print("int 10h")
            print("mov al,219 ; char")
            print("mov bl,{0} ; color".format(previous["color"]))
            print("mov cx,{0} ; repeticiones".format(i * 2))
            print("mov ah,09 ; service")
            print("int 10h")
            previous = pixel
            i = 1
    print("")

print(".exit")
print("end")
