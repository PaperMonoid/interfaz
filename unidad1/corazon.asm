.model small
.stack 100h
.data
.code
mov ax,03	; limpiar
int 10h

mov dl,40	; centro
mov dh,12
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,39	; hacia los lados
mov dh,11
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,41
mov dh,11
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,38	; mas hacia los lados
mov dh,10
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,42
mov dh,10
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,37	; mas hacia los lados
mov dh,10
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,43
mov dh,10
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,36	; lados y abajo
mov dh,11
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,44
mov dh,11
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,37	; diagonales para abajo
mov dh,12
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,43
mov dh,12
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,38	; diagonales mas para abajo
mov dh,13
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,42
mov dh,13
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,39	; diagonales casi final
mov dh,14
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,41
mov dh,14
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

mov dl,40	; final
mov dh,15
mov ah,02h
int 10h
mov dx,64
mov ah,02h
int 21h

.exit
end
