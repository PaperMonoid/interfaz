.model small
.stack 100h
.data
.code
mov ax,03 ; clean
int 10h
mov dh,12 ; row
mov dl,42 ; column
mov ah,02h
int 10h
mov al,64 ; char
mov bl,4 ; color
mov cx,1 ; repeticiones
mov ah,09 ; service
int 10h
.exit
end
