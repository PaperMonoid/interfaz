# Unidad 1
```asm
pintar:
mov al,64
mov bl,4
mov cx,1
mov ah,09
int 10h

limpiar:
mov ax,03
int 10h

salir:
mov ah,00h
int 16h
```

# Indice
+ No pintado
  + [x] caracter.asm Escribe el caracter @ en pantalla.
  + [x] ana.asm Escribe el nombre ana en pantalla.
  + [x] nombre.asm Escribe santiago en pantalla.
  + [x] centrado.asm Escribe el caracter @ en el centro de la pantalla.
  + [x] nombrec.asm Escribe santiago en el centro de la pantalla.
  + [x] nombrevh.asm Escribe santiago vertical y horizontalmente en el centro de la pantalla.
  + [x] circulo.asm Escribe un círculo en centro de la pantalla.
  + [x] corazon.asm Escribe un corazón en el centro de la pantalla.
  + [x] estrella.asm Escribe una estrella en el centro de la pantalla.
  + [x] feliz.asm Escribe una carita feliz en el centro de la pantalla.
  + [x] ajedrez.asm Escribe un tablero de ajedrez en la panalla.
+ Pintado
  + [x] carp.asm Escribe el caracter @ pintado rojo en pantalla.
  + [x] corazonp.asm Escribe un corazón rojo en el centro de la pantalla.
  + [x] felizp.asm Escribe una carita feliz amarilla en el centro de la pantalla.
  + [x] figurasp.asm, Escribe un círculo rojo en pantalla. Escribe un cuadro amarillo en pantalla. Escribe un rectangulo verde en pantalla. Escibe un triangulo azul en pantalla.
  + [x] banderap.asm Escribe la bandera de un país en el centro de la pantalla.

## Practica
+ Print screen
+ Código
+ Nombre
