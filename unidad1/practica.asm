.model small
.stack 100h
.data
.code
mov ax,03
int 10h
mov dh,0 ; row
mov dl,10 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,1 ; row
mov dl,10 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,1 ; row
mov dl,12 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,1 ; row
mov dl,14 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,2 ; row
mov dl,12 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,2 ; row
mov dl,16 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,2 ; row
mov dl,54 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,3 ; row
mov dl,12 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,8 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,3 ; row
mov dl,54 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,3 ; row
mov dl,56 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,4 ; row
mov dl,14 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,8 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,4 ; row
mov dl,38 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,4 ; row
mov dl,50 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,4 ; row
mov dl,54 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,4 ; row
mov dl,56 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,4 ; row
mov dl,58 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,4 ; row
mov dl,66 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,5 ; row
mov dl,14 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,5 ; row
mov dl,16 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,5 ; row
mov dl,28 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,5 ; row
mov dl,38 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,5 ; row
mov dl,50 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,5 ; row
mov dl,52 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,5 ; row
mov dl,56 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,5 ; row
mov dl,58 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,5 ; row
mov dl,64 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,5 ; row
mov dl,66 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,6 ; row
mov dl,16 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,26 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,6 ; row
mov dl,48 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,6 ; row
mov dl,52 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,6 ; row
mov dl,56 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,6 ; row
mov dl,58 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,6 ; row
mov dl,60 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,6 ; row
mov dl,62 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,15 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,6 ; row
mov dl,66 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,7 ; row
mov dl,10 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,7 ; row
mov dl,18 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,7 ; row
mov dl,20 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,10 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,7 ; row
mov dl,30 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,8 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,7 ; row
mov dl,38 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,7 ; row
mov dl,46 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,7 ; row
mov dl,48 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,7 ; row
mov dl,52 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,7 ; row
mov dl,58 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,7 ; row
mov dl,60 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,7 ; row
mov dl,62 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,15 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,7 ; row
mov dl,66 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,8 ; row
mov dl,10 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,8 ; row
mov dl,12 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,8 ; row
mov dl,18 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,8 ; row
mov dl,20 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,8 ; row
mov dl,26 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,8 ; row
mov dl,30 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,8 ; row
mov dl,36 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,8 ; row
mov dl,46 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,8 ; row
mov dl,50 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,8 ; row
mov dl,56 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,8 ; row
mov dl,58 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,8 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,9 ; row
mov dl,12 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,9 ; row
mov dl,16 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,9 ; row
mov dl,18 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,9 ; row
mov dl,24 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,9 ; row
mov dl,26 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,10 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,9 ; row
mov dl,36 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,9 ; row
mov dl,40 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,9 ; row
mov dl,44 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,9 ; row
mov dl,46 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,9 ; row
mov dl,48 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,8 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,9 ; row
mov dl,56 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,9 ; row
mov dl,58 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,9 ; row
mov dl,60 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,9 ; row
mov dl,62 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,9 ; row
mov dl,64 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,10 ; row
mov dl,12 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,10 ; row
mov dl,14 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,10 ; row
mov dl,16 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,10 ; row
mov dl,18 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,10 ; row
mov dl,22 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,10 ; row
mov dl,26 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,10 ; row
mov dl,28 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,8 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,10 ; row
mov dl,36 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,10 ; row
mov dl,42 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,10 ; row
mov dl,44 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,10 ; row
mov dl,46 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,10 ; row
mov dl,48 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,10 ; row
mov dl,52 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,10 ; row
mov dl,54 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,10 ; row
mov dl,56 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,10 ; row
mov dl,58 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,11 ; row
mov dl,2 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,8 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,11 ; row
mov dl,12 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,11 ; row
mov dl,14 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,8 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,11 ; row
mov dl,22 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,11 ; row
mov dl,24 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,11 ; row
mov dl,28 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,11 ; row
mov dl,30 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,15 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,11 ; row
mov dl,32 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,11 ; row
mov dl,36 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,10 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,11 ; row
mov dl,46 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,11 ; row
mov dl,48 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,8 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,11 ; row
mov dl,56 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,11 ; row
mov dl,58 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,12 ; row
mov dl,4 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,8 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,12 ; row
mov dl,12 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,12 ; row
mov dl,16 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,12 ; row
mov dl,22 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,12 ; row
mov dl,26 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,12 ; row
mov dl,30 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,12 ; row
mov dl,34 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,12 ; row
mov dl,38 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,12 ; row
mov dl,40 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,12 ; row
mov dl,46 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,12 ; row
mov dl,48 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,8 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,12 ; row
mov dl,56 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,12 ; row
mov dl,58 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,13 ; row
mov dl,8 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,13 ; row
mov dl,12 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,10 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,13 ; row
mov dl,22 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,13 ; row
mov dl,28 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,13 ; row
mov dl,34 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,6 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,13 ; row
mov dl,36 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,13 ; row
mov dl,38 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,15 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,13 ; row
mov dl,40 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,13 ; row
mov dl,42 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,13 ; row
mov dl,44 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,13 ; row
mov dl,46 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,13 ; row
mov dl,48 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,8 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,13 ; row
mov dl,56 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,13 ; row
mov dl,58 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,14 ; row
mov dl,12 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,14 ; row
mov dl,16 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,14 ; row
mov dl,22 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,14 ; row
mov dl,28 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,14 ; row
mov dl,34 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,6 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,14 ; row
mov dl,36 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,14 ; row
mov dl,38 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,15 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,14 ; row
mov dl,40 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,14 ; row
mov dl,42 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,14 ; row
mov dl,44 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,14 ; row
mov dl,46 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,14 ; row
mov dl,48 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,8 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,14 ; row
mov dl,56 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,14 ; row
mov dl,58 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,10 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,15 ; row
mov dl,12 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,15 ; row
mov dl,14 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,8 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,15 ; row
mov dl,22 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,8 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,15 ; row
mov dl,30 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,15 ; row
mov dl,32 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,15 ; row
mov dl,36 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,6 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,15 ; row
mov dl,38 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,15 ; row
mov dl,40 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,15 ; row
mov dl,42 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,15 ; row
mov dl,46 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,15 ; row
mov dl,50 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,15 ; row
mov dl,56 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,15 ; row
mov dl,58 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,15 ; row
mov dl,64 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,15 ; row
mov dl,68 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,16 ; row
mov dl,10 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,16 ; row
mov dl,16 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,16 ; row
mov dl,18 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,16 ; row
mov dl,22 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,16 ; row
mov dl,24 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,16 ; row
mov dl,30 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,16 ; row
mov dl,34 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,8 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,16 ; row
mov dl,42 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,16 ; row
mov dl,44 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,16 ; row
mov dl,48 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,16 ; row
mov dl,62 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,16 ; row
mov dl,64 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,15 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,16 ; row
mov dl,66 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,16 ; row
mov dl,68 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,15 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,17 ; row
mov dl,8 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,17 ; row
mov dl,18 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,17 ; row
mov dl,22 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,17 ; row
mov dl,24 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,17 ; row
mov dl,28 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,17 ; row
mov dl,30 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,15 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,17 ; row
mov dl,32 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,17 ; row
mov dl,36 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,17 ; row
mov dl,44 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,17 ; row
mov dl,46 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,17 ; row
mov dl,64 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,15 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,17 ; row
mov dl,66 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,17 ; row
mov dl,68 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,15 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,18 ; row
mov dl,18 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,18 ; row
mov dl,22 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,18 ; row
mov dl,26 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,18 ; row
mov dl,28 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,7 ; color
mov cx,8 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,18 ; row
mov dl,36 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,18 ; row
mov dl,38 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,18 ; row
mov dl,46 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,19 ; row
mov dl,12 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,19 ; row
mov dl,14 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,19 ; row
mov dl,16 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,19 ; row
mov dl,24 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,19 ; row
mov dl,28 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,8 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,19 ; row
mov dl,36 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,19 ; row
mov dl,38 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,19 ; row
mov dl,40 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,19 ; row
mov dl,46 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,19 ; row
mov dl,50 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,20 ; row
mov dl,10 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,20 ; row
mov dl,12 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,20 ; row
mov dl,18 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,20 ; row
mov dl,28 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,20 ; row
mov dl,38 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,20 ; row
mov dl,48 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,12 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,20 ; row
mov dl,50 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,21 ; row
mov dl,8 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,8 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,21 ; row
mov dl,16 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,2 ; repeticiones
mov ah,09 ; service
int 10h

mov dh,22 ; row
mov dl,6 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,2 ; color
mov cx,6 ; repeticiones
mov ah,09 ; service
int 10h
mov dh,22 ; row
mov dl,12 ; column
mov ah,02h
int 10h
mov al,219 ; char
mov bl,0 ; color
mov cx,4 ; repeticiones
mov ah,09 ; service
int 10h

; writes s
mov dh,24 ; initial row
mov dl,40 ; initial column
mov ah,02h
int 10h
mov dx,83
mov ah,02h
int 21h

; writes a
mov dh,24 ; initial row
mov dl,41 ; initial column
mov ah,02h
int 10h
mov dx,65
mov ah,02h
int 21h

; writes n
mov dh,24 ; initial row
mov dl,42 ; initial column
mov ah,02h
int 10h
mov dx,78
mov ah,02h
int 21h

; writes t
mov dh,24 ; initial row
mov dl,43 ; initial column
mov ah,02h
int 10h
mov dx,84
mov ah,02h
int 21h

; writes i
mov dh,24 ; initial row
mov dl,44 ; initial column
mov ah,02h
int 10h
mov dx,73
mov ah,02h
int 21h

; writes a
mov dh,24 ; initial row
mov dl,45 ; initial column
mov ah,02h
int 10h
mov dx,65
mov ah,02h
int 21h

; writes g
mov dh,24 ; initial row
mov dl,46 ; initial column
mov ah,02h
int 10h
mov dx,71
mov ah,02h
int 21h

; writes o
mov dh,24 ; initial row
mov dl,47 ; initial column
mov ah,02h
int 10h
mov dx,79
mov ah,02h
int 21h
.exit
end
