radius = 12
width = 80
height = 24
half_width = width / 2
half_height = height / 2


def distance(a, b):
    return (a ** 2 - b ** 2) ** 0.5


print(".model small")
print(".stack 100h")
print(".data")
print(".code")
print("mov ax,03")
print("int 10h")

for x in range(0, width - 1):
    try:
        _x = x - half_width
        _y = distance(radius, _x)
        y1 = half_height + _y
        y2 = half_height - _y
        print("mov dl,{0}".format(x))
        print("mov dh,{0}".format(int(y1)))
        print("mov ah,02h")
        print("int 10h")
        print("mov dx,64")
        print("mov ah,02h")
        print("int 21h")
        print("mov dl,{0}".format(x))
        print("mov dh,{0}".format(int(y2)))
        print("mov ah,02h")
        print("int 10h")
        print("mov dx,64")
        print("mov ah,02h")
        print("int 21h")
    except:
        pass

print(".exit")
print("end")
