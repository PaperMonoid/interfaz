.model small
.stack 100h
.data
.code
; limpiar
mov ax,03
int 10h

mov dh,12 ; initial row
mov dl,40 ; initial column
mov ah,02h
int 10h
mov dx,83
mov ah,02h
int 21h ; writes s

mov dh,12 ; same row
mov dl,41 ; move column
mov ah,02h
int 10h
mov dx,65
mov ah,02h
int 21h ; writes a

mov dh,12 ; same row
mov dl,42 ; move column
mov ah,02h
int 10h
mov dx,78
mov ah,02h
int 21h ; writes n

mov dh,12 ; same row
mov dl,43 ; move column
mov ah,02h
int 10h
mov dx,84
mov ah,02h
int 21h ; writes t

mov dh,12 ; same row
mov dl,44 ; move column
mov ah,02h
int 10h
mov dx,73
mov ah,02h
int 21h ; writes i

mov dh,12 ; same row
mov dl,45 ; move column
mov ah,02h
int 10h
mov dx,65
mov ah,02h
int 21h ; writes a

mov dh,12 ; same row
mov dl,46 ; move column
mov ah,02h
int 10h
mov dx,71
mov ah,02h
int 21h ; writes g

mov dh,12 ; same row
mov dl,47 ; move column
mov ah,02h
int 10h
mov dx,79
mov ah,02h
int 21h ; writes o


mov dh,12 ; initial row
mov dl,40 ; initial column
mov ah,02h
int 10h
mov dx,83
mov ah,02h
int 21h ; writes s

mov dh,13 ; move row
mov dl,40 ; same column
mov ah,02h
int 10h
mov dx,65
mov ah,02h
int 21h ; writes a

mov dh,14 ; move row
mov dl,40 ; same column
mov ah,02h
int 10h
mov dx,78
mov ah,02h
int 21h ; writes n

mov dh,15 ; move row
mov dl,40 ; same column
mov ah,02h
int 10h
mov dx,84
mov ah,02h
int 21h ; writes t

mov dh,16 ; move row
mov dl,40 ; same column
mov ah,02h
int 10h
mov dx,73
mov ah,02h
int 21h ; writes i

mov dh,17 ; move row
mov dl,40 ; same column
mov ah,02h
int 10h
mov dx,65
mov ah,02h
int 21h ; writes a

mov dh,18 ; move row
mov dl,40 ; same column
mov ah,02h
int 10h
mov dx,71
mov ah,02h
int 21h ; writes g

mov dh,19 ; move row
mov dl,40 ; same column
mov ah,02h
int 10h
mov dx,79
mov ah,02h
int 21h ; writes o

.exit
end