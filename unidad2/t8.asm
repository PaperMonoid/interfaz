.model small
.stack 64
.data
	msjEdad db 'Ingrese una edad de dos digitos: $'
	unidades db ?
	decenas db ?
	msjMa db 10,13,'Es mayor de edad $'
	msjMe db 10,13,'Es menor de edad $'
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

leerEdad:
	mov ah,09h
	lea dx,msjEdad
	int 21h

	; decenas
	mov ah,01h
	int 21h
	sub al,48 ;ajuste manual, ascii to decimal
	mov decenas,al

	;unidades
	mov ah,01h
	int 21h
	sub al,48 ;ajuste manual
	mov unidades,al

cmpDecenas:
	mov al,decenas
	cmp al,1
	jg mayor
	jl menor
	je cmpUnidades

cmpUnidades:
	mov al,unidades
	cmp al,8
	jg mayor
	jl menor
	je mayor

menor:
	mov ah,09h
	lea dx,msjMe
	int 21h
	jmp salir

mayor:
	mov ah,09h
	lea dx,msjMa
	int 21h
	jmp salir

salir:
	mov ah, 00h
	int 16h

.exit
	end
