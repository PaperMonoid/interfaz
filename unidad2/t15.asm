.model small
.stack 64
.data
	msj db 'Blog de notas hasta S:',10,13,'$'
	caracter db ?
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

mensaje:
	mov ah,09h
	lea dx,msj
	int 21h

leerLetra:
	mov ah,1
	int 21h
	mov caracter,al
	jmp comparacion

comparacion:
	mov al,caracter

	cmp al,83		; S
	je salir
	cmp al,115		; s
	je salir

	jmp leerLetra

salir:
	mov ah, 00h
	int 16h

.exit
	end
