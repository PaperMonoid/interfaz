.model small
.stack 64
.data
	msjNumero db 10,13,'Ingresa un numero: ','$'
	num1 db ?
	num2 db ?
	msjSuma db 10,13,'Suma: = ','$'
	msjResta db 10,13,'Resta: = ','$'
	msjMultiplicacion db 10,13,'Multiplicacion: = ','$'
	msjDivision db 10,13,'Division: = ','$'
	decenas db ?
	unidades db ?
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

leerNumero1:
	mov ah,09h
	lea dx,msjNumero
	int 21h

	mov ah,01h
	int 21h
	sub al,30h
	mov num1,al

leerNumero2:
	mov ah,09h
	lea dx,msjNumero
	int 21h

	mov ah,01h
	int 21h
	sub al,30h
	mov num2,al

suma:
	mov ah,09h
	lea dx,msjSuma
	int 21h

	mov al,num1
	add al,num2
	aam

	mov decenas,ah
	mov unidades,al

	add decenas,30h
	add unidades,30h

	mov ah,02h
	mov dl,decenas
	int 21h

	mov ah,02h
	mov dl,unidades
	int 21h

resta:
	mov ah,09h
	lea dx,msjResta
	int 21h

	mov al,num1
	sub al,num2
	aam

	mov decenas,ah
	mov unidades,al

	add decenas,30h
	add unidades,30h

	mov ah,02h
	mov dl,decenas
	int 21h

	mov ah,02h
	mov dl,unidades
	int 21h

multiplicacion:
	mov ah,09h
	lea dx,msjMultiplicacion
	int 21h

	mov al,num1
	mov bl,num2
	mul bl
	aam

	mov decenas,ah
	mov unidades,al

	add decenas,30h
	add unidades,30h

	mov ah,02h
	mov dl,decenas
	int 21h

	mov ah,02h
	mov dl,unidades
	int 21h

division:
	mov ah,09h
	lea dx,msjDivision
	int 21h

	xor ax,ax

	mov al, num1
	mov bl,num2
	div bl
	aam

	mov decenas,ah
	mov unidades,al

	add decenas,30h
	add unidades,30h

	mov ah,02h
	mov dl,decenas
	int 21h

	mov ah,02h
	mov dl,unidades
	int 21h

salir:
	mov ah, 00h
	int 16h

.exit
	end
