.model small
.stack 64
.data
	msjmensaje db 'Ingrese un numero: ','$',10,13
	num dd ?,?
	num1 dt ?,?
	msjdd db 'dd: ','$',10,13
	msjdt db 'dt: ','$',10,13
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

mensaje:
	mov ah,09h
	lea dx,msjmensaje
	int 21h

leer:
	mov ah,01h
	int 21h
	sub ax,30h
	mov word ptr [num],ax
	mov word ptr [num1],ax

	mov ah,02h
	mov dl,0ah ;salto de línea
	int 21h

	mov ah,02h
	mov dl,0dh ;retorno de carro
	int 21h

DDDD:
	mov ah,09h
	lea dx,msjdd
	int 21h

	mov ah,02h
	mov dx,word ptr [num]
	add dx,30h
	int 21h

	mov ah,02h
	mov dl,0ah ;salto de línea
	int 21h
	mov ah,02h
	mov dl,0dh ;retorno de carro
	int 21h

DTDT:
	mov ax,@data
	mov ds,ax
	mov ah,09h
	lea dx,msjdt
	int 21h

	mov ah,02h
	mov dx,word ptr [num1]
	add dx,30h
	int 21h

salir:
	mov ah, 00h
	int 16h

.exit
	end
