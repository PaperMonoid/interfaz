.model small
.stack 64
.data
	msj db 10,13,'Ingresa numero: ','$'
	num1 DB ?
	num2 DB ?
	num3 DB ?
	numA DB ?
	numB DB ?
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

leerN1:
	mov ah,09h
	lea dx, msj
	int 21h

	mov ah, 3fh
	mov bx, 00
	mov cx, 1
	lea dx, num1
	int 21h


	mov al, num1
	sub al,48
	mov num1, al

leerN2:
	mov ah,09h
	lea dx, msj
	int 21h

	mov ah, 3fh
	mov bx, 00
	mov cx, 1
	lea dx, num2
	int 21h

	mov al, num2
	sub al,48
	mov num2, al

leerN3:
	mov ah,09h
	lea dx, msj
	int 21h

	mov ah, 3fh
	mov bx, 00
	mov cx, 1
	lea dx, num3
	int 21h

	mov al, num2
	sub al,48
	mov num2, al

	mov al, num1

	cmp al, num2
	jae salto1
	cmp al, num3
	jb numero1A
	jmp numero3A

salto1:
	mov al, num2
	cmp al, num3
	jb numero2A

numero3A:
	mov dl, num3
	mov numA, 3
	jmp salto2

numero2A:
	mov dl, num2
	mov numA, 2
	jmp salto2

numero1A:
	mov dl, num1
	mov numA, 1
	jmp salto2

salto2:
	add dl, 30h
	mov ah, 02h
	int 21h

	mov al, numA
	cmp al, 1
	je salto3

	mov al, numA
	cmp al, 2
	je salto4

salto5:
	mov al, num1
	cmp al, num2
	jb numero1B

	jmp numero2B

salto4:
	mov al, num1
	cmp al, num3
	jb numero1B

	jmp numero3B

salto3:
	mov al, num2
	cmp al, num3
	jb numero2B

numero3B:
	mov dl, num3
	mov numB, 3
	jmp salto6

numero2B:
	mov dl, num2
	mov numB, 2
	jmp salto6

numero1B:
	mov dl, num1
	mov numB, 1
	jmp salto6

salto6:
	add dl, 30h
	mov ah, 02h
	int 21h

	mov al, numA
	add al, numB
	mov numA, al

	mov al, numA
	cmp al, 3
	je numero3C

	mov al, numA
	cmp al, 4
	je numero2C

numero1C:
	mov dl, num1
	jmp salto7

numero2C:
	mov dl, num2
	jmp salto7

numero3C:
	mov dl, num3
	jmp salto7

salto7:
	add dl, 30h
	mov ah, 02h
	int 21h

salir:
	mov ah, 00h
	int 16h

.exit
	end
