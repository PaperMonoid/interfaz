.model small
.stack 64
.data
	n1 db ?
	n2 db ?
	n3 db ?
	msj db 10,13,'Ingrese un numero: ','$'
	mayorn1 db 10,13,'El primero es el mayor.','$'
	mayorn2 db 10,13,'El segundo es el mayor.','$'
	mayorn3 db 10,13,'El tercero es el mayor.','$'
	iguales db 10,13,'Todos son iguales','$'
.code
limpiar:
	mov ax,	03h
	int 10h

apuntador:
	mov ax, @data
	mov ds, ax

leerN1:
	mov ah,09h
	lea dx, msj
	int 21h

	mov ah, 3fh
	mov bx, 00
	mov cx, 1
	lea dx, n1
	int 21h

	mov al, n1
	sub al,48
	mov n1, al

leerN2:
	mov ah,09h
	lea dx, msj
	int 21h

	mov ah, 3fh
	mov bx, 00
	mov cx, 1
	lea dx, n2
	int 21h

	mov al, n2
	sub al,48
	mov n2, al

leerN3:
	mov ah,09h
	lea dx, msj
	int 21h

	mov ah, 3fh
	mov bx, 00
	mov cx, 1
	lea dx, n3
	int 21h

	mov al, n3
	sub al,48
	mov n3, al

compararN1VsN2:
	mov al,n1
	cmp al,n2
	jg compararN1VsN3
	jl compararN2VsN3
	jmp compararN1VsN3

compararN1VsN3:
	mov al,n1
	cmp al,n3
	jg msjN1Mayor
	jl compararN2VsN3
	jmp todosIguales

compararN2VsN3:
	mov al,n2
	cmp al,n3
	jg msjN2Mayor
	jl msjN3Mayor

msjN1Mayor:
	mov ah,09h
	lea dx, mayorn1
	int 21h
	jmp salir

msjN2Mayor:
	mov ah,09h
	lea dx, mayorn2
	int 21h
	jmp salir

msjN3Mayor:
	mov ah,09h
	lea dx, mayorn3
	int 21h
	jmp salir

todosIguales:
	mov ah,09h
	lea dx, iguales
	int 21h
	jmp salir

salir:
	mov ah, 00h
	int 16h

.exit
	end
