.model small
.stack 64
.data
	msj DB "Ingrese un numero 0 - 9 : ","$"
	num db ?
	br db 10,13,'','$'
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

mensaje:
	mov ah,09h
	lea dx,msj
	int 21h

	mov ah,01h
	int 21h
	sub al,48
	mov num,al

	mov ah,09h
	lea dx,br
	int 21h

	mov cx,10

ciclo:
	mov ax,11
	sub ax,cx
	mul num
	aam

	mov bx,ax

	mov ah,02h
	mov dl,bh
	add dl,48
	int 21h
	mov dl,bl
	add dl,48
	int 21h
	mov dl,' '
	int 21h

	loop ciclo

salir:
	mov ah, 00h
	int 16h

.exit
	end
