model small
.stack 64
.data
	msgNombre db 'Ingrese un nombre: $'
	nombre dd 25 dup ('$'),'$'

	msgEdad db 'Ingrese una edad: $'
	edad dt 10 dup ('$'),'$'

	msgnom db 'Nombre: $'
	msgeda db 'Edad: $'
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

leerNombre:
	mov ah,09h
	lea dx, msgNombre
	int 21h

	mov ah,3fh
	mov bx,00
	mov cx,25
	mov dx, offset nombre
	int 21h

leerEdad:
	mov ah,09h
	lea dx, msgEdad
	int 21h

	mov ah,3fh
	mov bx,00
	mov cx,10
	mov dx, offset edad
	int 21h

	mov ax,03
	int 10h

desplegarDatos:
	mov ah,09h
	lea dx, msgnom
	int 21h

	mov ah,09h
	lea dx, nombre
	int 21h

	mov ah,02h
	mov dl,0dh ;retorno de carro
	int 21h

	mov ah,09h
	lea dx, msgeda
	int 21h

	mov ah,09h
	lea dx, edad
	int 21h

salir:
	mov ah, 00h
	int 16h

.exit
	end
