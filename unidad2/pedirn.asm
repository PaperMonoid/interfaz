.model small
.stack 100h
.data
numero db 'Ingresa un numero: ', '$', 10, 13
variable db 100 DUP(0), '$'
.code
limpiar:
mov ax,03h
int 10h

; lee data
mov ax, @data
mov ds, ax

; imprime numero
mov ah, 09h
lea dx, numero
int 21h

; pide datos
mov ah, 3fh
mov bx, 00
mov cx, 100
lea dx, variable
int 21h

; imprime datos
mov ah, 09h
lea dx, variable
int 21h

; espera una tecla
mov ah, 00h
int 16h
.exit
end
