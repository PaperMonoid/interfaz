.model small
.stack 64
.data
	msj db 'Solo admite S y N:',10,13,'$'
	caracter db ?
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

mensaje:
	mov ah,09h
	lea dx,msj
	int 21h

leerLetra:
	mov ah,1
	int 21h
	mov caracter,al
	jmp comparar

comparar:
	mov al,caracter
	cmp al,83 ;S
	je leerLetra
	cmp al,115 ;s
	je leerLetra
	cmp al,78 ;N
	je leerLetra
	cmp al,110 ;n
	je leerLetra
	jmp salir

salir:
	mov ah, 00h
	int 16h
.exit
	end
