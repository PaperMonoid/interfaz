.model small
.stack 64
.data
	msgNum db 'Ingrese numero: ','$'
	msgPar db 10,13,'Es par ','$'
	msgImpar db 10,13,'Es impar ','$'
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

leerNumero:
	mov ah,09h
	lea dx,msgNum
	int 21h

	mov ah,01h
	int 21h
	sub al,30h

parOImpar:
	and ax,1
	jz par

impar:
	mov ah,9
	lea dx,msgImpar
	int 21h
	jmp salir

par:
	mov ah,9
	leA dx,msgPar
	int 21h
	jmp salir

salir:
	mov ah, 00h
	int 16h

.exit
	end
