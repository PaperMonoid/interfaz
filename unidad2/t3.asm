.model small
.stack 64
.data
	msjNombre db 'DANIEL SANTIAGO AGUILA TORRES $'
	msjCarrera db 'INGENIERIA EN SISTEMAS COMPUTACIONALES $'
	msjSemestre db '6to $'
	msjEdad db '21 $'
	msjFecha db '02/02/1998 $'
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

nombre:
	mov ah,02h
	mov dh,1; fila
	mov dl,1; columna
	int 10h

	mov ah,09h
	lea dx, msjNombre
	int 21h

carrera:
	mov ah,02h
	mov dh,2; fila
	mov dl,1; columna
	int 10h

	mov ah,09h
	lea dx, msjCarrera
	int 21h

semestre:
	mov ah,02h
	mov dh,3; fila
	mov dl,1; columna
	int 10h

	mov ah,09h
	lea dx, msjSemestre
	int 21h

edad:
	mov ah,02h
	mov dh,4; fila
	mov dl,1; columna
	int 10h

	mov ah,09h
	lea dx, msjEdad
	int 21h

fecha:
	mov ah,02h
	mov dh,5; fila
	mov dl,1; columna
	int 10h

	mov ah,09h
	lea dx, msjFecha
	int 21h

salir:
	mov ah, 00h
	int 16h

.exit
	end
