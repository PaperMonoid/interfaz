.model small
.stack 64
.data
	pedir db "Ingrese un numero: ", "$"
	msgPrimo db 10,13,"El numero es primo", "$"
	msgNoPrimo db 10,13,"El numero no es primo", "$"
	num DB ?
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

leerNumero:
	mov ah, 09h;
	lea dx, pedir;
	int 21h;

	mov ah, 01h
	int 21h
	sub al, 30h
	mov num, al

cmpPrimo:
	cmp al, 2
	JE esPrimo

	cmp al, 3
	JE esPrimo

	cmp al, 5
	JE esPrimo

	cmp al, 7
	JE esPrimo

	mov ah, 09
	lea dx, msgNoPrimo
	int 21h
	JMP salir

esPrimo:
	mov ah, 09
	lea dx, msgPrimo
	int 21h

salir:
	mov ah,00h
	int 16h

.exit
	end
