.model small
.stack 64
.data
	unidades db 1
	decenas db 1
	centenas db 1
	msj db 'Numeros del 0 al 100 5 en 5',10,13,'$'
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

mensaje:
	mov ah,09h
	Lea dx,msj
	int 21h

	mov cx,105

ciclo:
	mov ah,02h
	mov dl," "
	int 21h

	sub cx,4
	mov ax,101
	sub ax,cx
	aam

	mov unidades,al
	mov al,ah
	aam

	mov centenas,ah
	mov decenas,al

	mov ah,02h
	mov dl,centenas
	add dl,48
	int 21h
	mov dl,decenas
	add dl,48
	int 21h
	mov dl,unidades
	add dl,48
	int 21h

	loop ciclo

salir:
	mov ah, 00h
	int 16h

.exit
	end
