.model small
.stack 64
.data
	msjSaludo db 'Hola mundo!', '$'
.code
limpiar:
	mov ax, 03h
	int 10h

saludo:
	mov dx, offset msjSaludo
	mov ah,09h
	int 21h

salir:
	mov ah, 00h
	int 16h

.exit
	end
