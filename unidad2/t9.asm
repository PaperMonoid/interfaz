.model small
.stack 64
.data
	msj db 10,13,'Esta cadena se repetira 5 veces','$'
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

	mov cx,5

ciclo:
	mov ah,09h
	lea dx,msj
	int 21h
	loop ciclo

salir:
	mov ah, 00h
	int 16h

.exit
	end
