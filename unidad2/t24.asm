.model small
.stack 64
.data
	msj db 'Menu: ','$'
	msjArchivo db 10,13,'1: Archivo $'
	msjBuscar db 10,13,'2: Buscar $'
	msjSalir db 10,13,'3: Salir $'
	msjOpcion db 10,13,'Ingrese una opcion: ','$'
	br db '',10,13,'$'
	num db ?
	msjClicArchivo db 10,13,'Selecciono Archivo $'
	msjClicBuscar db 10,13,'Selecciono Buscar $'
	msjClicSalir db 10,13,'Selecciono Salir $'
.code
apuntador:
	mov ax, @data
	mov ds,ax

limpiar:
	mov ax, 03h
	int 10h

menu:
	mov ah,09h
	lea dx,msj
	int 21h

	mov ah,09h
	lea dx,msjArchivo
	int 21h

	mov ah,09h
	lea dx,msjBuscar
	int 21h

	mov ah,09h
	lea dx,msjSalir
	int 21h

	mov ah,09h
	lea dx,msjOpcion
	int 21h

leerNumero:
	mov ah,01h
	int 21h
	sub al,30h
	mov num,al

	mov ah,09h
	lea dx,br
	int 21h

comparar:
	mov al,num

	cmp al,1
	je imprimirMsjClicArchivo

	cmp al,2
	je imprimirMsjClicBuscar

	cmp al,3
	je imprimirMsjClicSalir

	jmp limpiar

imprimirMsjClicArchivo:
	mov ah,09h
	lea dx,msjClicArchivo
	int 21h

	jmp salir

imprimirMsjClicBuscar:
	mov ah,09h
	lea dx,msjClicBuscar
	int 21h

	jmp salir

imprimirMsjClicSalir:
	mov ah,09h
	lea dx,msjClicSalir
	int 21h

	jmp salir

salir:
	mov ah, 00h
	int 16h

.exit
	end
