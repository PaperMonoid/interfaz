.model small
.stack 64
.data
	msj db 10,13,'Ingrese un numero: $'
	msj2 db 10,13,'Ingrese un numero: $'
	unidades db ?
	decenas db ?
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

mensaje:
	mov ah,09h
	lea dx,msj
	int 21h
	jmp leerNumero

mensajeOtroNumero:
	mov ah,09h
	lea dx,msj2
	int 21h
	jmp leerNumero

leerNumero:
	mov ah,01h
	int 21h
	sub al,48
	mov decenas,al

	mov ah,01h
	int 21h
	sub al,48
	mov unidades,al

comparar:
	mov al,decenas
	cmp al,1
	jl mensajeOtroNumero

salir:
	mov ah, 00h
	int 16h

.exit
	end
