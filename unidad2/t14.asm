.model small
.stack 64
.data
	msj db 'Ingrese un nombre: ','$'
	nombre db 20 DUP('$'), '$'
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

leerNombre:
	mov ah,09h
	lea dx, msj
	int 21h

	mov ah,3fh
	mov bx,00
	mov cx,20
	lea dx,nombre
	int 21h

	mov cx,20

ciclo:
	mov ah,09h
	lea dx,nombre
	int 21h
	loop ciclo

.exit
	end
