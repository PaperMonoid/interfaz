.model small
.stack 64
.data
	br db 10,13,'','$'
	num db 5
	msj db 10,13,'Ingrese un numero: ','$'
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

leerNumero:
	mov ah,09h
	lea dx,msj
	int 21h

	mov ah,01h
	int 21h
	sub al,30h
	mov num,al

	mov cl,9
ciclo:
	cmp cl,num
	je esNumero

	mov ah,09h
	lea dx,br
	int 21h

	mov ah,02h
	mov dl,cl
	add dl,30h
	int 21h

	cmp cl,0
	je salir

	dec cl
	jmp ciclo

esNumero:
	dec cl
	jmp ciclo

salir:
	mov ah, 00h
	int 16h

.exit
	end
