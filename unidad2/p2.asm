.model small
.stack 100h
.data
	nombre db 'Ingresa tu nombre: ', '$', 10, 13
	variable1 db 'Hola, soy  ', '$'
	variable db 10 DUP(0), '$'
	variable2 db ' y yo, no voy a copiar esta practica', '$'
.code
limpiar:
	mov ax,03h
	int 10h

	; lee data
	mov ax, @data
	mov ds, ax

	; imprime nombre
	mov ah, 09h
	lea dx, nombre
	int 21h

	; pide datos
	mov ah, 3fh
	mov bx, 00
	mov cx, 10
	mov dx, offset[variable]
	int 21h

	; inicia el registro contador en 20
	mov cx, 20

ciclo:
	mov ah,09h
	mov dx,offset[variable1]
	int 21h

	mov ah,09h
	mov dx,offset[variable]
	int 21h

	mov ah,09h
	mov dx,offset[variable2]
	int 21h

	loop ciclo

	; espera una tecla
	mov ah, 00h
	int 16h

.exit
	end
