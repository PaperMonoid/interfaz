.model small
.stack 64
.data
	msj db 'Numeros pares del 0 al 20',10,13,'$'
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

mensaje:
	mov ah,09h
	Lea dx,msj
	int 21h

	mov cx,20

ciclo:
	mov ah,02h
	mov dl," "
	int 21h

	dec cx
	mov ax,21
	sub ax,cx
	aam

	mov bx,ax

	mov ah,02h
	mov dl,bh
	add dl,48
	int 21h
	mov dl,bl
	add dl,48
	int 21h

	loop ciclo

salir:
	mov ah, 00h
	int 16h

.exit
	end
