.model small
.stack 100h
.data
nombre db 'Daniel Santiago', 10, 'Aguila Torres', 10, '$'
.code
limpiar:
mov ax,03h
int 10h

; lee data
mov ax, @data
mov ds, ax

; imprime nombre
mov ah, 09h
lea dx, nombre
int 21h

; espera una tecla
mov ah, 00h
int 16h
.exit
end
