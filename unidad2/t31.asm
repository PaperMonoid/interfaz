.model small
.stack 64
.data
	msj db 'Ingrese un numero','$'
	br db '',10,13,'$'
	num db ?
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

mensaje:
	mov ah,09h
	lea dx,msj
	int 21h

leerNumero:
	mov ah,01h
	int 21h
	sub al,30h
	mov num,al

	mov ah,09h
	lea dx,br
	int 21h

cuadrado:
	mov al,num
	mov bl,num
	mul bl
	aam

imprimir:
	mov bx,ax
	mov ah,02h
	mov dl,bh
	add dl,30h
	int 21h
	mov dl,bl
	add dl,30h
	int 21h

salir:
	mov ah, 00h
	int 16h

.exit
	end
