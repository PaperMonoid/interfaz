.model small
.stack 64
.data
	msgSaludo db 'HOLA MUNDO $'
.code
limpiar:
	mov ax, 03h
	int 10h
apuntador:
	mov ax, @data
	mov ds,ax
saludo:
	mov ah,09h
	lea dx, msgSaludo
	int 21h
salir:
	mov ah, 00h
	int 16h
.exit
	end
