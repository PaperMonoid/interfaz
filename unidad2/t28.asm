.model small
.stack 64
.data
	msj db 'Suma numeros del 1 - 10: $'
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

mensaje:
	mov ah,09h
	Lea dx,msj
	int 21h

	mov ax,0
	mov cx,10

ciclo:
	add ax,cx
	loop ciclo

imprimir:
	aam
	mov bx,ax

	mov ah,02h
	mov dl,bh
	add dl,48
	int 21h
	mov dl,bl
	add dl,48
	int 21h

salir:
	mov ah, 00h
	int 16h

.exit
	end
