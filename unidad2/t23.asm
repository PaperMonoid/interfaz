.model small
.stack 100h
.data
	mensaje db 'Ingresa una letra: ', '$', 10, 13
	msgConsonante db 'Es consonante', '$', 10, 13
	msgVocal db 'Es vocal', '$', 10, 13
.code
limpiar:
	mov ax,03h
	int 10h

apuntador:
	mov ax, @data
	mov ds, ax

leerLetra:
	mov ah, 09h
	lea dx, mensaje
	int 21h

	mov ah, 1
	int 21h

comparacion:
	mov bl, al

	cmp bl, 97 ; a
	je vocal
	cmp bl, 65 ; A
	je vocal
	cmp bl, 101 ; e
	je vocal
	cmp bl, 69 ; E
	je vocal
	cmp bl, 105 ; i
	je vocal
	cmp bl, 73 ; I
	je vocal
	cmp bl, 111 ; o
	je vocal
	cmp bl, 79 ; O
	je vocal
	cmp bl, 117 ; u
	je vocal
	cmp bl, 85 ; U
	je vocal

	mov ah, 09h
	lea dx, msgConsonante
	int 21h

	jmp detener

vocal:
	mov ah, 09h
	lea dx, msgVocal
	int 21h

detener:
	mov ah, 00h
	int 16h

.exit
	end
