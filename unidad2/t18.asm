.model small
.stack 100h
.data
	opcion db 0

	mensaje db 'Ingrese un numero del 1 al 7: ', '$', 10, 13
	msgLunes db 10, 13, 'Lunes', '$'
	msgMartes db 10, 13, 'Martes', '$'
	msgMiercoles db 10, 13, 'Miercoles', '$'
	msgJueves db 10, 13, 'Jueves', '$'
	msgViernes db 10, 13, 'Viernes', '$'
	msgSabado db 10, 13, 'Sabado', '$'
	msgDomingo db 10, 13, 'Domingo', '$'
.code
limpiar:
	mov ax,03h
	int 10h

apuntador:
	mov ax, @data
	mov ds, ax

dia:
	mov ah, 09h
	lea dx, mensaje
	int 21h

	mov ah, 01h
	int 21h
	sub al, 30h
	mov opcion, al

comparacion:
	cmp al, 1
	je lunes
	cmp al, 2
	je martes
	cmp al, 3
	je miercoles
	cmp al, 4
	je jueves
	cmp al, 5
	je viernes
	cmp al, 6
	je sabado
	cmp al, 7
	je domingo

lunes:
	mov ah, 09
	lea dx, msgLunes
	int 21h
	jmp detener

martes:
	mov ah, 09
	lea dx, msgMartes
	int 21h
	jmp detener

miercoles:
	mov ah, 09
	lea dx, msgMiercoles
	int 21h
	jmp detener

jueves:
	mov ah, 09
	lea dx, msgJueves
	int 21h
	jmp detener

viernes:
	mov ah, 09
	lea dx, msgViernes
	int 21h
	jmp detener

sabado:
	mov ah, 09
	lea dx, msgSabado
	int 21h
	jmp detener

domingo:
	mov ah, 09
	lea dx, msgDomingo
	int 21h
	jmp detener

detener:
	mov ah, 00h
	int 16h

.exit
	end
