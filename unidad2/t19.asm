.model small
.stack 100h
.data
	opcion db 0

	mensaje db 'Ingrese un numero del 1 al 12: ', '$', 10, 13
	msgEnero db 10, 13, 'Enero', '$'
	msgFebrero db 10, 13, 'Febrero', '$'
	msgMarzo db 10, 13, 'Marzo', '$'
	msgAbril db 10, 13, 'Abril', '$'
	msgMayo db 10, 13, 'Mayo', '$'
	msgJunio db 10, 13, 'Junio', '$'
	msgJulio db 10, 13, 'Julio', '$'
	msgAgosto db 10, 13, 'Agosto', '$'
	msgSeptiembre db 10, 13, 'Septiembre', '$'
	msgOctubre db 10, 13, 'Octubre', '$'
	msgNoviembre db 10, 13, 'Noviembre', '$'
	msgDiciembre db 10, 13, 'Diciembre', '$'
.code
limpiar:
	mov ax,03h
	int 10h

apuntador:
	mov ax, @data
	mov ds, ax

dia:
	mov ah, 09h
	lea dx, mensaje
	int 21h

	mov ah, 01h
	int 21h
	sub al, 30h
	mov opcion, al

comparacion:
	cmp al, 1
	je enero
	cmp al, 2
	je febrero
	cmp al, 3
	je marzo
	cmp al, 4
	je abril
	cmp al, 5
	je mayo
	cmp al, 6
	je junio
	cmp al, 7
	je julio
	cmp al, 8
	je agosto
	cmp al, 9
	je septiembre
	cmp al, 10
	je octubre
	cmp al, 11
	je noviembre
	cmp al, 12
	je diciembre

enero:
	mov ah, 09
	lea dx, msgEnero
	int 21h
	jmp detener

febrero:
	mov ah, 09
	lea dx, msgFebrero
	int 21h
	jmp detener

marzo:
	mov ah, 09
	lea dx, msgMarzo
	int 21h
	jmp detener

abril:
	mov ah, 09
	lea dx, msgAbril
	int 21h
	jmp detener

mayo:
	mov ah, 09
	lea dx, msgMayo
	int 21h
	jmp detener

junio:
	mov ah, 09
	lea dx, msgJunio
	int 21h
	jmp detener

julio:
	mov ah, 09
	lea dx, msgJulio
	int 21h
	jmp detener

agosto:
	mov ah, 09
	lea dx, msgAgosto
	int 21h
	jmp detener

septiembre:
	mov ah, 09
	lea dx, msgSeptiembre
	int 21h
	jmp detener

octubre:
	mov ah, 09
	lea dx, msgOctubre
	int 21h
	jmp detener

noviembre:
	mov ah, 09
	lea dx, msgNoviembre
	int 21h
	jmp detener

diciembre:
	mov ah, 09
	lea dx, msgDiciembre
	int 21h
	jmp detener

detener:
	mov ah, 00h
	int 16h

.exit
	end
