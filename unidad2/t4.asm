.model small
.stack 64
.data
	msjNombre db 'Ingrese un nombre: $'
	nombre db 25 dup ('$'),'$'

	msjCarrera db 'Ingrese una carrera: $'
	carrera db 45 dup ('$'),'$'

	msjSemestre db 'Ingrese un semestre: $'
	semestre db 10 dup ('$'),'$'

	msjEdad db 'Ingrese una edad: $'
	edad dt 10 dup ('$'),'$'

	msjFecha db 'Ingrese cumpleanos: $'
	fecha db 60 dup ('$'),'$'
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

leerNombre:
	mov ah,09h
	lea dx, msjNombre
	int 21h

	mov ah,3fh
	mov bx,00
	mov cx,25
	mov dx, offset nombre
	int 21h

leerCarrera:
	mov ah,09h
	lea dx, msjCarrera
	int 21h

	mov ah,3fh
	mov bx,00
	mov cx,25
	mov dx, offset carrera
	int 21h

leerSemestre:
	mov ah,09h
	lea dx, msjSemestre
	int 21h

	mov ah,3fh
	mov bx,00
	mov cx,25
	mov dx, offset semestre
	int 21h

leerEdad:
	mov ah,09h
	lea dx, msjEdad
	int 21h

	mov ah,3fh
	mov bx,00
	mov cx,25
	mov dx, offset edad
	int 21h

leerFecha:
	mov ah,09h
	lea dx, msjFecha
	int 21h

	mov ah,3fh
	mov bx,00
	mov cx,25
	mov dx, offset fecha
	int 21h

imprimirDatos:
	mov ax,03
	int 10h

	mov ah,09h
	lea dx, nombre
	int 21h

	mov ah,09h
	lea dx, carrera
	int 21h

	mov ah,09h
	lea dx, semestre
	int 21h

	mov ah,09h
	lea dx, edad
	int 21h

	mov ah,09h
	lea dx, fecha
	int 21h

salir:
	mov ah, 00h
	int 16h
.exit
	end
