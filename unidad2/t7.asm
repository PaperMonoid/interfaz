.model small
.stack 64
.data
	num1 db ?

	msgnum db 10,13,'Ingrese un numero: ','$'
	msg db 10,13,'El numero es menor que 10.','$'
	msg2 db 10,13,'El numero no es menor que 10.','$'
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

leerNumeroUno:
	mov ah,09h
	lea dx,msgnum
	int 21h

	mov ah,3fh
	mov bx,00
	mov cx,1
	lea dx,num1
	int 21h

	; ajuste
	mov al,num1
	sub al,48
	mov num1,al

comparar:
	; comparacion
	mov al,num1
	cmp al,9
	je iguales
	jl menor
	jg mayor

mayor:
	mov ah,09h
	lea dx,msg2
	int 21h
	jmp salir

menor:
	mov ah,09h
	lea dx,msg
	int 21h
	jmp salir

iguales:
	mov ah,09h
	lea dx,msg
	int 21h
	jmp salir

salir:
	mov ah, 00h
	int 16h

.exit
	end
