.model small
.stack 64
.data
	msj db 'Numeros del 0 al 10',10,13,'$'
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

mensaje:
	mov ah,09h
	Lea dx,msj
	int 21h

	mov cx,11

ciclo:
	mov ah,02h
	mov dl," "
	int 21h

	mov ax,11
	sub ax,cx
	aam

	mov bx,ax

	mov ah,02h
	mov dl,bh
	add dl,48
	int 21h
	mov dl,bl
	add dl,48
	int 21h

	loop ciclo

salir:
	mov ah, 00h
	int 16h

.exit
	end
