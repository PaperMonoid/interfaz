.model small
.stack 64
.data
	msj db 10,13,'Ingrese un numero: ','$'
	num db ?
	msjIncremento db 10,13,'Incremento: ','$'
	msjDecremento db 10,13,'Decremento: ','$'
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

mensaje:
	mov ah,09h
	lea dx, msj
	int 21h

leer:
	mov ah,01h
	int 21h
	sub al,30h
	mov num,al

incremento:
	mov ah,09h
	lea dx, msjIncremento
	int 21h

	mov al,num
	inc al
	aam
	mov bx,ax
	mov ah,02h
	mov dl,bh
	add dl,30h
	int 21h
	mov dl,bl
	add dl,30h
	int 21h

decremento:
	mov ah,09h
	lea dx, msjDecremento
	int 21h

	mov al,num
	dec al
	aam
	mov bx,ax
	mov ah,02h
	mov dl,bh
	add dl,30h
	int 21h
	mov dl,bl
	add dl,30h
	int 21h

salir:
	mov ah, 00h
	int 16h

.exit
	end
