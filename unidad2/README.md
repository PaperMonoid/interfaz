# Unidad 2
```asm
.data

; cadenas
nombre db 'monica', '$', 10, 13

; espacio para cadenas
variable db 100 DUP(0), '$'
variable db 100 DUP(?), '$'
variable db 100 DUP = ' ', '$'

; ciclo
mov cx, 9

ciclo:
mov ah, 02h
mov dx, cx
add dx, 30h ; imprime valor numérico
int 21h

loop ciclo

apuntador:
mov ax, @data
mov ds, ax

imprimir:
mov ah, 09h
lea dx, nombre
int 21h

imprimir2:
mov ah, 09h
offset nombre
int 21h

imprimir3:
mov ah, 3fh
mov bx, 00
mov cx, 100
mov dx, offset[cadena]
int 21h

leer:
mov ah, 3fh
mov bx, 00
mov cx, 100
lea dx, edad
int 21h
```

# Indice
+ [x] hola.asm, Muestra hola mundo en pantalla.
+ [x] nombre.asm, Muestra el nombre completo en pantalla.
+ [x] nombrec.asm, Muestra nombre completo en pantalla con salto de línea en apellidos.
+ [x] pedir.asm, Pide el nombre al usuario y lo despliega en pantalla en apellidos.
+ [x] pedirn.asm, Pide un número al usuario y lo despliega en pantalla.

* Indice de exposiciones
+ [x] p1.asm, Muestra un nombre de forma diagonal.
+ [x] p2.asm, Pide el nombre al usuario y lo despliega en pantalla 20 veces como "Hola, soy <insertar su nombre aquí> y yo, no voy a copiar esta práctica".
+ [x] p3.asm, Imprimir los números del 5 al 1 con ajuste y sin ajuste.
+ [x] p4.asm, Pedir 3 números e imprimir el mayor.
+ [x] p5.asm, Imprimir números en pantalla sin N.
+ [x] p6.asm, Incrementar y decrementar.
+ [x] p7.asm, Suma, resta, multiplicación y división.
+ [x] p8.asm, Pila.
+ [x] p9.asm, Hex a decimal.
+ [x] p10.asm, Operadores lógicos.
+ [x] p11.asm, Desplazamiento.
+ [x] p12.asm, Rotación.
+ [x] p13.asm, Archivo en disco.

* Indice de ejercicios
+ [x] t1.asm, Saludo con Lea.
+ [x] t2.asm, Saludo con offset.
+ [x] t3.asm, Desplegado de datos predeterminados.
+ [x] t4.asm, Desplegado de datos pedidos por teclado.
+ [x] t5.asm, Pedir un número y desplegarlo como número DD y Dt.
+ [x] t6.asm, Pedir datos con dd y dt .
+ [x] t7.asm, Capturar un número y comparar si es menor a 10.
+ [x] t8.asm, Capturar edad,  desplegar si es mayor o menor.
+ [x] t9.asm, Programa que imprima una cadena de caracteres 5 veces.
+ [x] t10.asm, a..Z.
+ [x] t11.asm, z...a.
+ [x] t12.asm, 0...9.
+ [x] t13.asm, 9...0.
+ [x] t14.asm, Pide el nombre y lo despliega 20 veces.
+ [x] t15.asm, Blog de notas hasta ingresar S.
+ [x] t16.asm, Pide un número del 1 al 9 y diga si es primo.
+ [x] t17.asm, Pide un número y diga si es par o impar.
+ [x] t18.asm, Pide un número del 1 al 7 y diga el día de la semana correspondiente.
+ [x] t19.asm, Pide un número del 1 al 12 y diga el nombre del mes correspondiente.
+ [x] t20.asm, Pide 3 números y los muestre en pantalla de menor a mayor.
+ [x] t21.asm, Pide 3 números y los muestre en pantalla de mayor a menor.
+ [x] t22.asm, Sólo permite introducir los caracteres S y N.
+ [x] t23.asm, Pide una letra y detecte si es una vocal.
+ [x] t24.asm, Muestra un menú que contemple las opciones “Archivo”, “Buscar” y “Salir”, en caso de que no se introduzca una opción correcta se notificará por pantalla.
+ [x] t25.asm, Escribir un programa que realice un ciclo y muestre en pantalla del 1 al 10.
+ [x] t26.asm, Escribir un programa que visualice en pantalla los números pares entre 0 y 20.
+ [x] t27.asm, Escribir un programa que visualice en pantalla los números múltiplos de 5 comprendidos entre 1 y 100.
+ [x] t28.asm, Escribir un programa que sume los números comprendidos entre 1 y 10.
+ [x] t29.asm, Escribir un programa que genere la tabla de multiplicar de un número introducido por el teclado.
+ [x] t30.asm, Escribir un programa que pida un número y si el que se introduce por el teclado es menor de 10 que vuelva a solicitarlo.
+ [x] t31.asm, Programa que calcule el cuadrado de un número ingresado por teclado.
+ [x] t32.asm, Programa que pida dos números por teclado de  dos dígitos cada número ingresado y le aplique las operaciones básicas suma, resta, multiplicacion y división.
+ [x] t33.asm, Programa que pida un numero y muestra la tabla de multiplicar que corresponda en el siguiente formato  1*1=1.

* Extra
+ [x] archivo.asm, Crear, abrir, escribir y cerrar un archivo.
