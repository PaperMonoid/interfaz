.model small
.stack 64
.data
	mascara db ?
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

operacion:
	mov mascara, 0FFh
	xor mascara, 0F0h

imprimir:
	mov al, mascara
	aam

	mov bx,ax

	mov ah,02h
	mov dl,bh
	add dl,48
	int 21h
	mov dl,bl
	add dl,48
	int 21h

	;pushf			; inserta banderas al stack
	;pop ax			; mueve banderas al registro ax
	;and ax, mascara

	; mov ah,09h
	; lea dx, msgSaludo
	; int 21h
salir:
	mov ah, 00h
	int 16h
.exit
	end
