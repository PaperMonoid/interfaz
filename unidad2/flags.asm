.model small
.stack 64
.data
	nz db 00040h
	msgZf0 db "ZF = 0$"
	msgZf1 db "ZF = 1$"
.code
limpiar:
	mov ax, 03h
	int 10h

apuntador:
	mov ax, @data
	mov ds,ax

mascara:
	xor nz, 00040h

imprimir:
	mov bx, 1
	sub bx, 1

	pushf			; inserta banderas en la pila
	pop ax			; mueve banderas de la pila a ax
	and al, nz		; modifica zf a 0
	push ax			; inserta ax en la pila
	popf			; mueve ax de la pila a las banderas

	je zf1
	jmp zf0

zf1:
	mov ah,09h
	lea dx, msgZf1
	int 21h
	jmp salir

zf0:
	mov ah,09h
	lea dx, msgZf0
	int 21h
	jmp salir

salir:
	mov ah, 00h
	int 16h
.exit
	end
