.model small
.stack 100h
.data
.code
limpiar:
mov ax,03h
int 10h

; inicia el registro contador en 5
mov cx, 5

ciclo:
mov ah, 02h
mov dx, cx ; imprime contador
int 21h

loop ciclo

; inicia el registro contador en 5
mov cx, 5

cicloajuste:
mov ah, 02h
mov dx, cx
add dx, 48
int 21h

loop cicloajuste

; espera una tecla
mov ah, 00h
int 16h

.exit
end
