.model small
.stack 100h
.data
hola db 'Hola mundo', '$', 10, 13
.code
limpiar:
mov ax,03h
int 10h

; lee data
mov ax, @data
mov ds, ax

; imprime hola
mov ah, 09h
lea dx, hola
int 21h

; espera una tecla
mov ah, 00h
int 16h
.exit
end