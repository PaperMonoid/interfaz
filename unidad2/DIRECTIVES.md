# Assembly 8086 directives
* DB. Define Byte(s).
* DW. Define Word(s).
* DQ. Define QuadWord(s).
* DT. Define Tenbyte(s).
* DD. Define Double Word(s).

# Resources
* [Introduction to 8086 Assembly Language](https://www.shsu.edu/~csc_tjm/fall2003/cs272/intro_to_asm.html)
* [8086 Assembly Instruction Set](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=15&ved=2ahUKEwjLq_7piZThAhUJj1QKHVVSCkYQFjAOegQIBBAC&url=http%3A%2F%2Fusers.utcluj.ro%2F~elupu%2FCurs%2Ffileloader.php%3FfileName%3Dupload%2FCursuri%2FMicroprocesoare_I%2FCurs_6%2F8086_instruction_set.pdf&usg=AOvVaw1OrRhU4xQrK0AAm8bKu82a)
